        package com.montanez.aplicationaws

        import android.Manifest
        import android.content.Intent
        import android.content.pm.PackageManager
        import android.graphics.Bitmap
        import android.os.Build
        import androidx.appcompat.app.AppCompatActivity
        import android.os.Bundle
        import android.provider.MediaStore
        import android.util.Log
        import android.widget.Button
        import android.widget.ImageView
        import android.widget.Toast
        import androidx.activity.result.contract.ActivityResultContracts
        import androidx.annotation.RequiresApi
        import androidx.core.content.ContextCompat
        import com.amazonaws.auth.BasicAWSCredentials
        import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
        import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
        import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
        import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
        import com.amazonaws.regions.Region
        import com.amazonaws.regions.Regions
        import com.amazonaws.services.s3.AmazonS3Client
        import java.io.ByteArrayOutputStream
        import java.io.File
        import java.lang.Exception

        //esto es solo para efectos academicos por que debe estar protegido
        //AWS S3
object  Constans{

}

        class MainActivity : AppCompatActivity() {

            var imageView: ImageView? = null;
            var btnPhoto: Button? = null;
            //AWS S3


            @RequiresApi(Build.VERSION_CODES.M)
            override fun onCreate(savedInstanceState: Bundle?) {
                super.onCreate(savedInstanceState)
                setContentView(R.layout.activity_main)

                imageView = findViewById(R.id.imageView);
                btnPhoto = findViewById(R.id.btPhoto);


                val solicitaPermisos = arrayOf(
                    Manifest.permission.CAMERA
                )
                requestPermissions(solicitaPermisos,1)

                if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    tomaFoto()
                }else{

                    //TO DO: No se está mostrando!! Verificar hilos
                    Toast.makeText(this, R.string.msg_error_camera, Toast.LENGTH_LONG)
                }

            }


            private fun tomaFoto() {

                val fotografo = registerForActivityResult(
                    ActivityResultContracts.StartActivityForResult()
                ){
                        result-> if(result.resultCode== RESULT_OK){
                    val rextras= result.data!!.extras
                    val imagen= rextras!!.get("data") as Bitmap?
                    imageView!!.setImageBitmap(imagen)


                    // convert file bitmat to file
                    val bytes = ByteArrayOutputStream()
                    imagen!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                    val archivo= File(
                        externalCacheDir!!.absoluteFile.toString() + File.separator + "fabianMontanez1.jpg")

                     archivo.createNewFile()

                    val   temporal = archivo.outputStream()
                            temporal.write(bytes.toByteArray())
                            temporal.close()

                    //Guardar imagen y enviar a S3
                   val subida =  cargarImagen(archivo)

                    }
                }
                fotografo.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))

            }


            private fun cargarImagen(imagen: File){
                var credenciales:BasicAWSCredentials=
                    BasicAWSCredentials(Constans.ACCESS_ID, Constans.SECRET_KEY)
                var clienteS3: AmazonS3Client = AmazonS3Client(credenciales, Region.getRegion(
                    Regions.US_EAST_2) )
                //object for sent
                val transportador = TransferUtility.builder().context(applicationContext).s3Client(clienteS3).build()

                val observador: TransferObserver = transportador.upload(
                    Constans.BUCKET_NAME,
                    Constans.SECRET_KEY,
                    imagen
                )

            observador.setTransferListener(
                object: TransferListener{
                    override fun onStateChanged(id: Int, state: TransferState?) {
                        if(state==TransferState.COMPLETED){
                            Log.d("msg", "Transferencia Exitosa")
                        }else if(state==TransferState.FAILED){
                            Log.d("msg", "Transferencia Fallida")
                        }
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        if(bytesCurrent==bytesTotal){
                            Toast.makeText(applicationContext,"Imagen Cargada !!! ", Toast.LENGTH_SHORT)
                        }
                    }

                    override fun onError(id: Int, ex: Exception?) {
                        Log.d("error", ex.toString())
                    }

                }
            )

            }

        }